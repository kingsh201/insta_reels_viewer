

## 0.0.1 - 2024-01-08
* Initial Open Source release.

## 0.0.2 - 2024-01-08
* Remove flutter warnings.

## 0.0.3 - 2024-01-08
* import issue fixed

## 0.0.4 - 2024-01-09
* import issue fixed

## 0.0.5 - 2024-01-09
* icon issue fixed

## 0.0.6 - 2024-01-09
* icon issue fixed

## 0.0.7 - 2024-02-02
* example added

## 0.0.8 - 2024-02-11
* example issue fixed