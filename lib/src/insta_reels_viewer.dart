import 'package:flutter/material.dart';
import 'package:insta_reels_viewer/insta_reels_viewer.dart';

class InstaReelsViewer extends StatefulWidget {
  final List<ReelsModel> reelList;
  final Function(ReelsModel reelData)? onClickCommentIcon;
  final Function(ReelsModel reelData)? onClickShareIcon;
  final Function(ReelsModel reelData)? onLike;
  final Function(ReelsModel reelData)? onUnLike;
  final Function(ReelsModel reelData)? onClickName;
  final Function(ReelsModel reelData)? onFollow;
  final Function(ReelsModel reelData)? onUnFollow;

  const InstaReelsViewer(
      {Key? key,
      required this.reelList,
      this.onClickCommentIcon,
      this.onClickShareIcon,
      this.onLike,
      this.onUnLike,
      this.onClickName,
      this.onFollow,
      this.onUnFollow})
      : super(key: key);

  @override
  State<InstaReelsViewer> createState() => _InstaReelsViewerState();
}

class _InstaReelsViewerState extends State<InstaReelsViewer> {
  List<ReelsModel> myrees1 = [];
  late List<CachedVideoPlayerController> _controllers;
  int _currentPage = 0;
  bool isMute = false;
  bool readMore = false;
  double? height = 20.0;
  double? likeHeight = 0.0;
  double? likeWidth = 0.0;

  @override
  void initState() {
    myrees1 = widget.reelList;

    // myrees1.add(
    //     ReelsModel(
    //   url: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
    //   likeCount: 578,
    //   commentCount: 200,
    //   sendCount: 70,
    //   isLiked: true,
    //   musicName: "my music name 1",
    //   musicImageUrl: "",
    //   description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //   createdAt: DateTime.now().toString(),
    //   updatedAt: DateTime.now().toString(),
    //   user: User(
    //     userName: "Raman Thakur",
    //     profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //     createdAt: DateTime.now().toString(),
    //     updatedAt: DateTime.now().toString(),
    //     isFollow: true
    //   ),
    //   reelComments: [
    //     ReelComments(
    //       cmment: "akkdfhkaljhflkajdf",
    //         createdAt: DateTime.now().toString(),
    //         updatedAt: DateTime.now().toString(),
    //         user: User(
    //           userName: "Vanit Dev",
    //           profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //           createdAt: DateTime.now().toString(),
    //           updatedAt: DateTime.now().toString(),
    //         )
    //     )
    //   ]
    //
    // )
    // );
    // myrees1.add(
    //
    //     ReelsModel(
    //     url: "https://assets.mixkit.co/videos/preview/mixkit-tree-with-yellow-flowers-1173-large.mp4",
    //     likeCount: 578,
    //         commentCount: 43,
    //         sendCount: 80,
    //     isLiked: false,
    //     musicName: "my music name 1",
    //     musicImageUrl: "",
    //     description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //     createdAt: DateTime.now().toString(),
    //     updatedAt: DateTime.now().toString(),
    //     user: User(
    //       userName: "Pankaj Thakur",
    //       profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //       createdAt: DateTime.now().toString(),
    //       updatedAt: DateTime.now().toString(),
    //         isFollow: false
    //     ),
    //     reelComments: [
    //       ReelComments(
    //           cmment: "akkdfhkaljhflkajdf",
    //           createdAt: DateTime.now().toString(),
    //           updatedAt: DateTime.now().toString(),
    //           user: User(
    //             userName: "Vanit Dev",
    //             profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //             createdAt: DateTime.now().toString(),
    //             updatedAt: DateTime.now().toString(),
    //               isFollow: true
    //           )
    //       )
    //     ]
    //
    // )
    // );
    // myrees1.add(
    //     ReelsModel(
    //     url:"https://assets.mixkit.co/videos/preview/mixkit-portrait-of-a-woman-in-a-pool-1259-small.mp4",
    //     likeCount: 578,
    //         commentCount: 30,
    //         sendCount: 7,
    //     isLiked: true,
    //     musicName: "",
    //     musicImageUrl: "",
    //     description: "",
    //     createdAt: DateTime.now().toString(),
    //     updatedAt: DateTime.now().toString(),
    //     user: User(
    //       userName: "Abhishek Shukla",
    //       profileUrl: "",
    //       createdAt: DateTime.now().toString(),
    //       updatedAt: DateTime.now().toString(),
    //         isFollow: true
    //     ),
    //     reelComments: [
    //       ReelComments(
    //           cmment: "akkdfhkaljhflkajdf",
    //           createdAt: DateTime.now().toString(),
    //           updatedAt: DateTime.now().toString(),
    //           user: User(
    //             userName: "Vanit Dev",
    //             profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //             createdAt: DateTime.now().toString(),
    //             updatedAt: DateTime.now().toString(),
    //               isFollow: true
    //           )
    //       )
    //     ]
    //
    // )
    // );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://assets.mixkit.co/videos/preview/mixkit-little-girl-with-bunny-tiara-in-the-garden-48601-large.mp4",
    //  likeCount: 578,
    //      commentCount: 40,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Satwinder Singh",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://assets.mixkit.co/videos/preview/mixkit-winter-fashion-cold-looking-woman-concept-video-39874-small.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://assets.mixkit.co/videos/preview/mixkit-eastern-egg-picnic-in-the-garden-48599-small.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Rajeev Thakur",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://assets.mixkit.co/videos/preview/mixkit-girl-in-neon-sign-1232-small.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Manpreet Singh",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Jatin Kumar",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //      isFollow: false
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url:"https://assets.mixkit.co/videos/preview/mixkit-portrait-of-a-fashion-woman-with-silver-makeup-39875-small.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Raman Thakur",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  )
    //  )
    //  ]
    //
    //  )
    //  );
    // myrees1.add(
    //  ReelsModel(
    //  url: "https://assets.mixkit.co/videos/preview/mixkit-silhouette-of-urban-dancer-in-smoke-33898-large.mp4",
    //  likeCount: 578,
    //  isLiked: true,
    //  musicName: "my music name 1",
    //  musicImageUrl: "",
    //  description: "this is demo descriptioin, adkf alkd adkjhfladhlfkja ajkldhfkahfkj  alkjfhaldjfh  akjdhfa jhkfhadhjfkjhfkjdhfd dfd",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Raman Thakur",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  ),
    //  reelComments: [
    //  ReelComments(
    //  cmment: "akkdfhkaljhflkajdf",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  user: User(
    //  userName: "Vanit Dev",
    //  profileUrl: "https://img.freepik.com/free-photo/young-indian-man-dressed-trendy-outfit-monitoring-information-from-social-networks_231208-2766.jpg",
    //  createdAt: DateTime.now().toString(),
    //  updatedAt: DateTime.now().toString(),
    //  )
    //  )
    //  ]
    //
    //  )
    //  );

    _controllers = List.generate(
      myrees1.length,
      (index) => CachedVideoPlayerController.network(myrees1[index].url ?? ""),
    );
    _initializeControllers();
    pageController?.addListener(_onPageChanged);
    super.initState();
  }

  @override
  void dispose() {
    pageController?.removeListener(_onPageChanged);
    pageController?.dispose();
    _controllers.forEach((controller) => controller.dispose());
    super.dispose();
  }

  void _initializeControllers() async {
    await Future.wait(
        _controllers.map((controller) => controller.initialize()));
    setState(() {});
  }

  void _onPageChanged() {
    final newPage = pageController?.page?.toInt() ?? 0;
    if (newPage != _currentPage) {
      _currentPage = newPage;
      // Preload next video

      if (_currentPage < myrees1.length - 1) {
        _controllers[_currentPage + 1].initialize().then((_) {
          setState(() {});
        });
        _controllers[_currentPage - 1].initialize().then((_) {
          setState(() {});
        });
      }
    }
  }

  void onDoubleTapLike(int index) {
    likeHeight = 55.0;
    likeWidth = 55.0;
    Future.delayed(Duration(milliseconds: 110)).then((value) {
      likeHeight = 45.0;
      likeWidth = 45.0;
      setState(() {});
    });
    Future.delayed(Duration(milliseconds: 500)).then((value) {
      likeHeight = 0.0;
      likeWidth = 0.0;
      setState(() {});
    });
    // if(myrees1[index].isLiked !=null){
    myrees1[index].isLiked = true;
    // }
    if (widget.onLike != null) {
      widget.onLike!(myrees1[index]);
    }

    setState(() {});
  }

  void readMoreFunc() {
    readMore = !readMore;
    if (readMore == true) {
      height = null; //MediaQuery.sizeOf(context).height/2;
    } else {
      height = 20.0;
    }
    setState(() {});
    // print("readMore");
    // print(readMore);
  }

  void followUnfollow(int index) {
    if (myrees1[index].user?.isFollow != null) {
      myrees1[index].user?.isFollow = !myrees1[index].user!.isFollow!;
      if (myrees1[index].user?.isFollow == true) {
        if (widget.onFollow != null) {
          widget.onFollow!(myrees1[index]);
        }
      } else {
        if (widget.onUnFollow != null) {
          widget.onUnFollow!(myrees1[index]);
        }
      }
      setState(() {});
    }
  }

  void likeUnlike(int index) {
    if (myrees1[index].isLiked != null) {
      myrees1[index].isLiked = !myrees1[index].isLiked!;
      if (myrees1[index].isLiked == true) {
        if (widget.onLike != null) {
          widget.onLike!(myrees1[index]);
        }
      } else {
        if (widget.onLike != null) {
          widget.onUnLike!(myrees1[index]);
        }
      }
      setState(() {});
    }
  }

  void muteUnmuteAll() {
    // print("isMute");
    // print(isMute);
    isMute = !isMute;
    // print("isMute");
    // print(isMute);
    // print("_currentPage");
    // print(_currentPage);

    if (isMute == true) {
      _controllers.forEach((element) {
        element.setVolume(0.0);
      });
    } else {
      _controllers.forEach((element) {
        element.setVolume(10.0);
      });
    }
    setState(() {});
  }

  PageController? pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body:
          // DropdownButton()
          Stack(
        children: [
          PageView.builder(
            itemCount: myrees1.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onDoubleTap: () {
                  onDoubleTapLike(index);
                },
                child: Stack(children: [
                  ReelsWidget(
                    playerController: _controllers[index],
                  ),
                  Positioned(
                      bottom: 10,
                      // right: 5,
                      child: Container(
                          // height: 50,
                          width: MediaQuery.sizeOf(context).width,
                          color: Colors.transparent,
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  myrees1[index].user != null
                                      ? GestureDetector(
                                          onTap: () {
                                            readMoreFunc();
                                          },
                                          child: Container(
                                            // color: Colors.green,
                                            // height: 50,
                                            //   width: 30,
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Row(
                                                  children: [
                                                    Container(
                                                      margin: EdgeInsets.only(
                                                          left: 10),
                                                      height: 35,
                                                      width: 35,
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Colors.amber,
                                                          image: myrees1[index]
                                                                      .user
                                                                      ?.profileUrl !=
                                                                  null
                                                              ? DecorationImage(
                                                                  image: NetworkImage(myrees1[index]
                                                                          .user!
                                                                          .profileUrl!
                                                                          .isEmpty
                                                                      ? "https://www.pngitem.com/pimgs/m/272-2720656_user-profile-dummy-hd-png-download.png"
                                                                      : myrees1[index].user?.profileUrl ??
                                                                          ""),
                                                                  fit: BoxFit
                                                                      .cover)
                                                              : DecorationImage(
                                                                  image: NetworkImage(
                                                                      "https://www.pngitem.com/pimgs/m/272-2720656_user-profile-dummy-hd-png-download.png"),
                                                                  fit: BoxFit.cover)),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(
                                                      myrees1[index]
                                                              .user
                                                              ?.userName ??
                                                          "",
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    ),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    GestureDetector(
                                                      onTap: () {
                                                        followUnfollow(index);
                                                      },
                                                      child: Container(
                                                        height: 25,
                                                        decoration:
                                                            BoxDecoration(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                  10.0,
                                                                ),
                                                                border: Border.all(
                                                                    color: Colors
                                                                        .white),
                                                                color: Colors
                                                                    .transparent),
                                                        child: Center(
                                                          child: Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10.0,
                                                                    right:
                                                                        10.0),
                                                            child: Text(
                                                              myrees1[index]
                                                                          .user
                                                                          ?.isFollow ==
                                                                      false
                                                                  ? "Follow"
                                                                  : "Following",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          left: 15.0),
                                                  child: Container(
                                                      // duration: Duration(milliseconds: 500),
                                                      width: MediaQuery.sizeOf(
                                                                  context)
                                                              .width *
                                                          0.79,
                                                      height: height,
//                                constraints: BoxConstraints(
//                                    maxWidth: MediaQuery.sizeOf(context).width*0.79,
// maxHeight: null
//
//                                ),
                                                      child: Text(
                                                        myrees1[index]
                                                                .description ??
                                                            "",
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            overflow: readMore ==
                                                                    false
                                                                ? TextOverflow
                                                                    .ellipsis
                                                                : null),
                                                      )),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                myrees1[index].musicName != null
                                                    ? Visibility(
                                                        visible: myrees1[index]
                                                                    .musicName !=
                                                                null &&
                                                            myrees1[index]
                                                                .musicName!
                                                                .isNotEmpty,
                                                        child: GestureDetector(
                                                          onTap: () {},
                                                          child: Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    left: 10),
                                                            height: 25,
                                                            decoration:
                                                                BoxDecoration(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      10.0,
                                                                    ),
                                                                    // border: Border.all(color: Colors.white),
                                                                    color: Colors
                                                                        .black
                                                                        .withAlpha(
                                                                            100)),
                                                            child: Center(
                                                              child: Padding(
                                                                padding: const EdgeInsets
                                                                        .only(
                                                                    left: 10.0,
                                                                    right:
                                                                        10.0),
                                                                child: Row(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Icon(
                                                                      Icons
                                                                          .music_note_rounded,
                                                                      color: Colors
                                                                          .white,
                                                                      size: 15,
                                                                    ),
                                                                    SizedBox(
                                                                      width: 10,
                                                                    ),
                                                                    Text(
                                                                      myrees1[index]
                                                                              .musicName ??
                                                                          "",
                                                                      style: TextStyle(
                                                                          color: Colors
                                                                              .white,
                                                                          fontSize:
                                                                              14),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    : SizedBox()
                                              ],
                                            ),
                                          ),
                                        )
                                      : SizedBox(),
                                  Spacer(),
                                  Column(
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                likeUnlike(index);
                                              },
                                              child: Container(
                                                  color: Colors.transparent,
                                                  child: myrees1[index]
                                                              .isLiked ==
                                                          false
                                                      ? Image.asset(
                                                          // "assets/heart_filled.png",
                                                          ImageConstants
                                                              .unlikeIcon,
                                                          color: Colors.white,
                                                          height: 25,
                                                          width: 25,
                                                          package:
                                                              "insta_reels_viewer",
                                                          errorBuilder:
                                                              (d, df, asdf) {
                                                            // print("RRRRRRRRR");
                                                            // print("${d}");
                                                            // print("${df}");
                                                            // print("${asdf}");
                                                            return Container(
                                                              height: 20,
                                                              width: 20,
                                                              color: Colors.red,
                                                            );
                                                          },
                                                        )
                                                      : Image.asset(
                                                          // "assets/heart_filled.png",
                                                          ImageConstants
                                                              .likeIcon,
                                                          color: Colors.red,
                                                          height: 25,
                                                          width: 25,
                                                          package:
                                                              "insta_reels_viewer",
                                                          errorBuilder:
                                                              (d, df, asdf) {
                                                            // print("RRRRRRRRR");
                                                            // print("${d}");
                                                            // print("${df}");
                                                            // print("${asdf}");
                                                            return Container(
                                                              height: 20,
                                                              width: 20,
                                                              color: Colors.red,
                                                            );
                                                          },
                                                        )),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              myrees1[index].likeCount == null
                                                  ? ""
                                                  : myrees1[index]
                                                          .likeCount
                                                          ?.toString() ??
                                                      "",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            GestureDetector(
                                              onTap: () {
                                                // print("object");
                                                if (widget.onClickCommentIcon !=
                                                    null) {
                                                  widget.onClickCommentIcon!(
                                                      myrees1[index]);
                                                }
                                              },
                                              child: Container(
                                                  color: Colors.transparent,
                                                  child: Image.asset(
                                                    // "assets/heart_filled.png",
                                                    ImageConstants.commentIcon,
                                                    package:
                                                        "insta_reels_viewer",
                                                    color: Colors.white,
                                                    height: 32,
                                                    width: 32,
                                                    errorBuilder:
                                                        (d, df, asdf) {
                                                      return Container(
                                                        height: 20,
                                                        width: 20,
                                                        color: Colors.red,
                                                      );
                                                    },
                                                  )),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              myrees1[index].commentCount ==
                                                      null
                                                  ? ""
                                                  : myrees1[index]
                                                          .commentCount
                                                          ?.toString() ??
                                                      "",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            GestureDetector(
                                              onTap: () {},
                                              child: Container(
                                                  color: Colors.transparent,
                                                  child: Image.asset(
                                                    // "assets/heart_filled.png",
                                                    ImageConstants.sahreIcon,
                                                    package:
                                                        "insta_reels_viewer",
                                                    color: Colors.white,
                                                    height: 25,
                                                    width: 25,
                                                    errorBuilder:
                                                        (d, df, asdf) {
                                                      return Container(
                                                        height: 20,
                                                        width: 20,
                                                        color: Colors.red,
                                                      );
                                                    },
                                                  )),
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              myrees1[index].sendCount == null
                                                  ? ""
                                                  : myrees1[index]
                                                          .sendCount
                                                          ?.toString() ??
                                                      "",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(right: 10.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            Icon(
                                              Icons.more_horiz,
                                              color: Colors.white,
                                              size: 30,
                                            )
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              )
                            ],
                          ))),

                  //animated Like Icon
                  Align(
                    alignment: Alignment.center,
                    child: AnimatedContainer(
                      height: likeHeight,
                      width: likeWidth,
                      duration: Duration(milliseconds: 100),
                      child: Image.asset(
                        // "assets/heart_filled.png",
                        ImageConstants.likeIcon,
                        color: Colors.red,
                        package: "insta_reels_viewer",
                        errorBuilder: (d, df, asdf) {
                          return Container(
                            height: 20,
                            width: 20,
                            color: Colors.red,
                          );
                        },
                      ),
                      // curve: Curves.bounceInOut,
                    ),
                  )
                ]),
              );
              // return VideoPlayerScreen(videoUrl: videoUrls[index]);
            },
          ),
          //muteButton
          Positioned(
              top: 50,
              right: 10,
              child: GestureDetector(
                onTap: () {
                  muteUnmuteAll();
                },
                child: Container(
                    height: 30,
                    width: 30,
                    color: Colors.transparent,
                    child: Icon(
                      isMute == false
                          ? Icons.volume_up_outlined
                          : Icons.volume_off_outlined,
                      color: Colors.white,
                    )),
              )),
        ],
      ),
    );
  }
}
