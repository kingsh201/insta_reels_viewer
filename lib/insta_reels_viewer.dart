library insta_reels_viewer;

export 'src/components/reels_widget.dart';
export 'package:insta_reels_viewer/src/constants/image_constants.dart';
export 'package:insta_reels_viewer/src/model/reels_model.dart';
export 'package:cached_video_player/cached_video_player.dart';
export 'package:insta_reels_viewer/src/insta_reels_viewer.dart';
